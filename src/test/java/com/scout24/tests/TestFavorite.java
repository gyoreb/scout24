package com.scout24.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TestFavorite extends TestBase {

    @BeforeClass
    private void beforeClass() {
        loginPage = homePage.GoToLoginPage();
        loginPage.logIn(config.getUserEmail(), config.getUserPassword());
        assertEquals(homePage.getUserInfo(), config.getUserEmail(), "User is not signed in");
    }

    @Test
    public void testSetFavorite() {
        homePage.returnToHomePage();
        searchResultsPage = homePage.search();
        String carData = searchResultsPage.getAdvertisementTitle();
        searchResultsPage.addFirstResultToFavorites();
        favoritesPage = homePage.GoToFavoritesPage();
        String favoritesCarData = favoritesPage.getFirstFavoritesTitle();
        assertTrue(favoritesCarData.contains(carData), "Saved title is not matching. Expected: " +
                carData + "\n Actual: " + favoritesCarData);
    }
}
