package com.scout24.tests;

import com.scout24.pageobjects.pages.FavoritesPage;
import com.scout24.pageobjects.pages.HomePage;
import com.scout24.pageobjects.pages.LoginPage;
import com.scout24.pageobjects.pages.SearchResultsPage;
import com.scout24.utils.Config;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

@Slf4j
public class TestBase {

    public static Config config = Config.getInstance();
    public static LoginPage loginPage;
    public static HomePage homePage;
    public static SearchResultsPage searchResultsPage;
    public static FavoritesPage favoritesPage;
    public static WebDriver driver;
    public static WebDriverWait waitDriver;

    @BeforeSuite
    public void beforeSuite() {
        System.setProperty("webdriver.gecko.driver", config.getChromeDriverPath());
        driver = new FirefoxDriver();

        // Maximize the window browser
        Dimension dimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(dimension);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Go to initial page
        driver.get(config.getBaseUrl());
        initPageObjects();
        waitDriver = new WebDriverWait(driver, 10);
    }

    private void initPageObjects() {
        // Instantiate the page objects classes
        homePage = PageFactory.initElements(driver, HomePage.class);
    }

    @AfterSuite
    public void afterSuite() {
        driver.close();
    }
}
