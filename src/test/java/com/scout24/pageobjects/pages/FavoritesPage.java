package com.scout24.pageobjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.testng.Assert.assertFalse;

public class FavoritesPage {
    @FindBy(xpath = "//li[contains(@class, 'object-list-item')]")
    private List<WebElement> favorites;

    public String getFirstFavoritesTitle() {
        assertFalse(favorites.isEmpty(), "Car was not added to the favorites");
        return favorites.get(0).findElement(By.className("title-tertiary")).getText();
    }
}
