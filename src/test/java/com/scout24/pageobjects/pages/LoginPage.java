package com.scout24.pageobjects.pages;

import com.scout24.tests.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends TestBase {

    @FindBy(id = "LogOnModel_UserName")
    private WebElement emailInputField;

    @FindBy(id = "LogOnModel_Password")
    private WebElement passwordInputField;

    @FindBy(id = "LoginSubmit")
    private WebElement loginButton;

    public LoginPage LoginPage() {
        return PageFactory.initElements(driver, LoginPage.class);
    }

    public void logIn(String email, String password) {
        fillEmail(email);
        fillPassword(password);
        loginButton.click();
    }

    private void fillEmail(String email) {
        emailInputField.clear();
        emailInputField.sendKeys(email);
    }

    private void fillPassword(String password) {
        passwordInputField.clear();
        passwordInputField.sendKeys(password);
    }
}
