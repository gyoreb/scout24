package com.scout24.pageobjects.pages;

import com.scout24.tests.TestBase;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

@Slf4j
public class HomePage extends TestBase {

    @FindBy(className = "user-info")
    private WebElement loginMenu;

    @FindBy(className = "login")
    private WebElement loginName;

    @FindBy(id = "logo")
    private WebElement logo;

    @FindBy(xpath = "//a[@class='base-nav-link btn btn-lg btn-cta btn-block mb-4 mb-sm-0']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@href='https://www.autoscout24.ch/de/member/favorites']")
    private WebElement favorites;

    public LoginPage GoToLoginPage() {
        loginMenu.click();
        return PageFactory.initElements(driver, LoginPage.class);
    }

    public FavoritesPage GoToFavoritesPage() {
        waitDriver.until(ExpectedConditions.elementToBeClickable(loginMenu));
        loginMenu.click();
        waitDriver.until(ExpectedConditions.elementToBeClickable(favorites));
        favorites.click();
        return PageFactory.initElements(driver, FavoritesPage.class);
    }

    public void returnToHomePage() {
        logo.click();
    }

    public String getUserInfo() {
        return loginName.getText();
    }

    public SearchResultsPage search() {
        searchButton.click();
        return PageFactory.initElements(driver, SearchResultsPage.class);
    }
}