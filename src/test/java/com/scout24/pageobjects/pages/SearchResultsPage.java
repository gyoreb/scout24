package com.scout24.pageobjects.pages;

import com.scout24.tests.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends TestBase {

    @FindBy(xpath = "//article[@class='vehicle-card card mb-4 mb-sm-5']")
    private List<WebElement> searchResults;

    public String getAdvertisementTitle() {
        return searchResults.get(0).findElement(By.xpath("//span[@class='text-info font-weight-bold d-flex align-items-center vehicle-title text mb-3 mb-md-4']")).getText();
    }

    public void addFirstResultToFavorites() {
        searchResults.get(0).findElement(By.tagName("button")).click();
    }
}
