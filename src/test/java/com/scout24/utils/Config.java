package com.scout24.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Data
@Slf4j
public class Config {
    private static Config instance = null;
    private String baseUrl;
    private String chromeDriverPath;
    private String userEmail;
    private String userPassword;

    private Config() {
        // Exists only to defeat instantiation.
    }

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
            instance.propertiesSet();
        }
        return instance;
    }

    private void propertiesSet() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/test/java/com/scout24/recources/resources.properties"));
        } catch (IOException e) {
            log.error("Cannot read properties file!");
        }
        baseUrl = properties.getProperty("BASEURL");
        chromeDriverPath = properties.getProperty("chrome.driver.path");
        userEmail = properties.getProperty("userEmail");
        userPassword = properties.getProperty("userPassword");
    }
}
